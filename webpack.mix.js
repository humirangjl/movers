const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig(webpack => {
    return {
        plugins: [
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
                Popper: ['popper.js', 'default'],
            })
        ]
    };
});

mix.scripts([
    'resources/frontend/js/vendor/jquery-v3.3.1.min.js',
    'resources/frontend/js/vendor/popper.min.js',
    'resources/frontend/js/vendor/bootstrap.min.js',
    'resources/frontend/js/owl.carousel.min.js',
    'resources/frontend/js/jquery.nice-select.min.js',
    'resources/frontend/js/jquery-ui.js',
    'resources/frontend/js/jquery.star-rating-svg.min.js',
    'resources/frontend/js/main.js',
    ], 'public/frontend/js/app.js')
    .styles([
        'resources/frontend/css/bootstrap.min.css',
        'resources/frontend/css/font-awesome.min.css',
        'resources/frontend/css/owl.carousel.min.css',
        'resources/frontend/css/nice-select.css',
        'resources/frontend/css/jquery-ui.css',
        'resources/frontend/css/star-rating-svg.css',
        'resources/frontend/css/main.css',
        'resources/frontend/css/responsive.css',
    ], 'public/frontend/css/app.css')
    .copyDirectory('resources/frontend/fonts','public/frontend/fonts')
    .copyDirectory('resources/frontend/img','public/frontend/img')
    .copyDirectory('resources/static/images','public/images')
    .copyDirectory('resources/backend','public/backend')
    .js('resources/js/app.js', 'public/backend/js')
    .sass('resources/sass/app.scss', 'public/backend/css')
    .copyDirectory('resources/static/images','public/images')
    .browserSync('http://192.168.0.196:8000')
    .version()
    .sourceMaps();
