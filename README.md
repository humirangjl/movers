## Setup:

All you need is to run these commands:

```bash

git config --global user.email "your-email@gmail.com" 

git config --global user.name "Your Name" 

ssh-keygen # Optional

cat ~/.ssh/id_rsa.pub # Optional 

git clone git@gitlab.com:humirangjl/fresh-laravel-5.8.git your_folder_name

cd your_folder_name

composer install

npm install

php artisan vendor:publish #then input 0 and hit enter

cp .env.example

nano .env # change the database name, username and password

php artisan key:generate

php artisan migrate:fresh --seed

npm run production # To compile assets for prod

php artisan serve

```
  

**Note:**

Username/Email: admin@gmail.com

Password: 12345


## Included Packages:

#### Laravel (php):

  

*  [Laravel Framework](https://github.com/laravel/laravel/) (5.8.*)

*  [Forms & HTML](https://github.com/laravelcollective/html) : for forms

*  [Laravel Debugbar](https://github.com/barryvdh/laravel-debugbar) : for debugging

*  [Intervention Image](https://github.com/intervention/image) : image handling and manipulation

  

#### JS plugins:

  

* All ADMINATOR plugins [here](https://github.com/puikinsh/Adminator-admin-dashboard#built-with)

*  [sweetalert2](https://github.com/limonte/sweetalert2)

*  [Axios](https://github.com/mzabriskie/axios)

  
#### How can I use custom CSS and JS ?
The current architecture of assets are inspired from [adminator](https://github.com/puikinsh/Adminator-admin-dashboard/tree/master/src/assets/scripts), so all assets(css and js) are located in [resources](https://github.com/kossa/laradminator/tree/master/resources). if you want to add new component, like [select2](https://select2.org/) juste create file like [`resources/js/select2/index.js`](https://github.com/kossa/laradminator/blob/master/resources/js/select2/index.js), and don't forget to load it in [bootstrap.js](https://github.com/kossa/laradminator/blob/master/resources/js/bootstrap.js#L54)

If you want to update the CSS, you can put them directly in [`resources/sass/app.scss`](https://github.com/kossa/laradminator/blob/master/resources/sass/app.scss#L72) or create new file under `resources/sass` and import it in [resources/sass/app.scss
](https://github.com/kossa/laradminator/blob/master/resources/sass/app.scss#L5)

> VERY IMPORTANT: Any change you make on resources you have to run : `npm run dev` or  `npm run prod`.
> If you have a lot of changes it's much better to run: `npm run watch` to watch any changes on your files, take a look on [browersync](https://laravel.com/docs/5.8/mix#browsersync-reloading)


#### Create new CRUD

Creating CRUD in your application is the job you do most. Let's create Post CRUD:

  

* Add new migration and model : `php artisan make:model Post -m`

* Open migration file and add your columns

* Create PostsController : `php artisan make:controller PostController`. fill your resource (you can use UserController with some changes) or, if you are a lazy developer like me, use a [snippet](https://github.com/kossa/st-snippets/blob/master/kossa_php/Laravel/lcontroller.sublime-snippet) and make only 2 changes

* Duplicate `resource/views/admin/users` folder to `posts`, make changes in `index.php`, `create.blade.php`, `edit.blade.php`