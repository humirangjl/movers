<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Slug;

class Review extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'user_id',
        'name',
        'title',
        'description',
        'rating',
        'customer_service',
        'recommended',
        'date',
        'state',
        'slug',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password', 'remember_token',
    // ];

    /*
    |------------------------------------------------------------------------------------
    | Relationships
    |------------------------------------------------------------------------------------
    */
    public function company(){
        return $this->hasOne('App\Models\Company', 'id', 'company_id');
    }

    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false, $id = null)
    {
        $commun = [
            'company_id'        => "required",
            'name'              => "required",
            'state'             => "required",
            'title'             => "required",
            'description'       => 'required',
            'rating'            => 'required',
            'honesty'           => 'required',
            'preparedness'      => 'required',
            'knowledgeable'     => 'required',
            'overall_service'   => 'required',
            'recommended'       => 'required',
            'date'              => 'required',
        ];

        return $commun;
    }

    /*
    |------------------------------------------------------------------------------------
    | Backend Update
    |------------------------------------------------------------------------------------
    */
    // public static function changeCredentials($request)
    // {
    //     $admin = Auth::user();
    //     if($admin->role == 10){
    //         $admin->email = $request->new_email;
    //         $admin->save();
    //         if($request->new_password){
    //             $admin->password = $request->new_password;
    //             $admin->save();
    //         } return true;
    //     } return false;
    // }

    /*
    |------------------------------------------------------------------------------------
    | Attributes
    |------------------------------------------------------------------------------------
    */
    public function getCustomerServiceAttribute($value)
    {
        return json_decode($value, true);
    }

    public function setCustomerServiceAttribute($value)
    {
        $this->attributes['customer_service'] = json_encode($value);
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Slug::createSlug($value, 'Review');
    }
}
