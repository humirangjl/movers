<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use File, Image;
use App\Slug;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'website',
        'address',
        'description',
        'email',
        'phone_number',
        'social_media',
        'logo',
        'city',
        'state',
        'zip_code',
        'country',
        'operation',
        'slug',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password', 'remember_token',
    // ];

    /*
    |------------------------------------------------------------------------------------
    | Relationships
    |------------------------------------------------------------------------------------
    */
    public function reviews(){
        return $this->hasMany('App\Models\Review', 'company_id', 'id')->latest();
    }

    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false, $id = null)
    {
        $commun = [
            'name'              => "required|unique_with:companies, city, state,$id",
            'city'              => "required",
            'state'             => "required",
            'country'           => "required",
            'website'           => 'required',
            'operation'         => 'required',
        ];

        if ($update) {
            return $commun;
        }

        return array_merge($commun, [
            'name'  => "required|unique_with:companies, city, state",
            'logo'  => 'required|image',
        ]);
    }

    /*
    |------------------------------------------------------------------------------------
    | Backend Update
    |------------------------------------------------------------------------------------
    */
    // public static function changeCredentials($request)
    // {
    //     $admin = Auth::user();
    //     if($admin->role == 10){
    //         $admin->email = $request->new_email;
    //         $admin->save();
    //         if($request->new_password){
    //             $admin->password = $request->new_password;
    //             $admin->save();
    //         } return true;
    //     } return false;
    // }

    /*
    |------------------------------------------------------------------------------------
    | Attributes
    |------------------------------------------------------------------------------------
    */
    public function getLogoAttribute($value)
    {
        if (!$value) {
            return 'http://placehold.it/160x160';
        }

        return '/uploads/company_logo/'.rawurlencode($value);
    }

    public function setLogoAttribute($photo)
    {
        $path = public_path().'/uploads/company_logo/';
        if(!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }
        $file_name = $photo;
        $file = Image::make($file_name);
        $generated_filename = $file_name->getClientOriginalName();
        $file->save($path.''.$generated_filename);
        $this->attributes['logo'] = $generated_filename;
    }

    public function getOperationAttribute($value)
    {
        return explode(', ', $value);
    }

    public function setOperationAttribute($value)
    {
        $this->attributes['operation'] = implode(', ', $value);
    }

    public function getSocialMediaAttribute($value)
    {
        return json_decode($value, true);
    }

    public function setSocialMediaAttribute($value)
    {
        $this->attributes['social_media'] = json_encode($value);
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Slug::createSlug($value, 'Company');
    }
}
