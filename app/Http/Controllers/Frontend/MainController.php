<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;
use App\Slug;
use Carbon\Carbon;
use App\Models\Company;
use App\Models\Review;

class MainController extends Controller
{
    function getIndex(Request $request)
    {
        if($request->ajax()){
            $this->data['reviews'] = Review::orderBy('date', 'desc')->paginate('10');
            if($request->has('offset')){
                $this->data['timezone_name'] = timezone_name_from_abbr("", $request->offset*60, false);
                $there = $this->data['reviews'][0]->created_at;
                $here = Carbon::parse($there)->timezone($this->data['timezone_name']);
                $here->addSeconds($there->offset - $here->offset);
                dd($there->diffForHumans($there));
                $content = View::make('frontend.includes.append.top-ten-reviews', $this->data)->render();
                return response()->json(["result" => 'success', "html" => $content]);
            } return $this->data;
        }
        $this->data['reviews'] = Review::orderBy('date', 'desc')->paginate('10');
        return view('frontend.index', $this->data);
    }

    function getFindMover()
    {
        return view('frontend.find-mover');
    }

    function getRatingAndReports()
    {
        return view('frontend.rating-and-reports');
    }

    function getAboutUs()
    {
        return view('frontend.about-us');
    }

    function getMoverLogin()
    {
        return view('frontend.mover-login');
    }

    function getMoversProfile()
    {
        return view('frontend.mover-profile');
    }

    function getQuote()
    {
        return view('frontend.quote');
    }

    function getWriteAReview()
    {
        return view('frontend.write-a-review');
    }

    function getFixedDatabase()
    {
        Review::orderBy('id')->chunk(100, function ($companies) {
            foreach ($companies as $company) {
                $new_company = Review::find($company->id);

                $new_company->slug   =  Slug::createSlug($company->title, 'Review');
                $new_company->save();

                echo $new_company->slug.'<br>';
            }
        });
    }

    function getMoverregistration(){
            return view('frontend.registration');
    }
}
