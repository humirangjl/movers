<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\quote;
class QuoteController extends Controller
{
   public  function storequote(Request $request){
    $this->validate($request,[
        'zip_code' =>                                                                'required',
        'state' =>                                                                    'required',
        'city' =>                                                                    'required',
        'moving_size' =>                                                             'required',
        'movingdate' =>                                                              'required',
        'fullname' =>                                                                'required', 
        'email' =>                                                                   'required',
        'phone_number' =>                                                            'required']);

     $quote = new quote;
     $quote->zip_code = $request->input('zip_code');
     $quote->state = $request->input('state');
     $quote->moving_size = $request->input('moving_size');
     $quote->city = $request->input('city');
     $quote->additional_size = $request->input('additional_moving');
     $quote->date = $request->input('movingdate');
     $quote->fullname =$request->input('fullname');
     $quote->email = $request->input('email');
     $quote->phone_number = $request->input('phone_number');
     $quote->save();
     return back()->with('success','Thank you!,Your Information has been received.'); 
    }
}
