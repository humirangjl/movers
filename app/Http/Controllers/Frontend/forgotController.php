<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Mail;
class forgotController extends Controller
{
    function forgot(){
        return view('frontend.forgot-password');
    }

    function password(Request $request){
        $user = User::whereEmail($request->email)->first();
        if($user == null){
            return redirect()->back()->with(['error' =>'Email not exists']);    
        }
        $user = Sentinel::findById($user->id);
        $reminder = Reminder::exists($user) ? : Reminder::create($user);
        $this->sendEmail($user,$reminder->code);
        return redirect()->back()->with(['success' => 'Reset code sent to your email']);
       
    }

    public function sendEmail($user,$code){
          Mail::send('frontend.forget',['user' => $user,'code' => $code],function($message) use ($user){
                   $message->to($user->email);
                   $message->Subject("$user->name   ",'reset your password');
          });
    }

    function reset($email,$code){
        $user = User::whereEmail($email)->first();
        if($user == null){
            return redirect()->back()->with(['error' =>'Email not exists']);    
        }
         $user = Sentinel::findById($user->id);
         $reminder = Reminder::exists($user);
       if($reminder){
           if($code == $reminder->code){
                return view('frontend.resetpassword')->with(['user' => $user,'code' => $code]);
           }
           else{
               return view('/');
           }
       }  
       else{
        return redirect()->back()->with(['error' =>'Time Expired']);  
       }
    }

     function resetPassword(Request $request,$email,$code){
              $this->validate($request,['password' => 'required|min:6|max:12|confirmed','password_confirmation' => 'required|min:6|max:12']);
              $user = User::whereEmail($email)->first();
              if($user == null){
                  return redirect()->back()->with(['error' =>'Email not exists']);    
              }
               $user = Sentinel::findById($user->id);
               $reminder = Reminder::exists($user);
            if($reminder){
                if($code == $reminder->code){
                    Reminder::complete($user,$code, $request->password);
                        return redirect('/mover-login')->with(['success','Password reset. Please Login with a new password']);
                }
                else{
                    return view('/');
                }
            }  
            else{
                return redirect()->back()->with(['error' =>'Time Expired']);  
            }

     }
}
