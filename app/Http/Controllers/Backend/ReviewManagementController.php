<?php

namespace App\Http\Controllers\Backend;

use App\Models\Company;
use App\Models\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReviewManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $this->data['companies'] = Company::orderBy('name')->limit(10)->get();
        $this->data['items'] = Review::latest()->get();
        return view('admin.review-management.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->ajax()){
            $companies = Company::select('name', 'id', 'state')->where('name', 'like', '%'.$request->q.'%')->orderBy('name')->limit(10)->get();
            return $companies;
        }
        $this->data['companies'] = Company::select('name', 'id', 'state')->orderBy('name')->limit(10)->get();
        return view('admin.review-management.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Review::rules());
        $request->request->add([
            'customer_service' => [
                'honesty'           => $request->honesty,
                'knowledgeable'     => $request->knowledgeable,
                'preparedness'      => $request->preparedness,
                'overall_service'   => $request->overall_service,
            ],
            'slug'  => $request->title,
        ]);
        Review::create($request->all());
        return back()->withSuccess(trans('app.success_store'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if($request->ajax()){
            $companies = Company::select('name', 'id', 'state')->where('name', 'like', '%'.$request->q.'%')->orderBy('name')->limit(10)->get();
            return $companies;
        }
        $this->data['companies'] = Company::select('name', 'id', 'state')->orderBy('name')->limit(10)->get();
        $this->data['item'] = Review::findOrFail($id);

        return view('admin.review-management.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Review::rules(true, $id));

        $item = Review::findOrFail($id);

        $item->update($request->all());

        return redirect()->route(ADMIN . '.review-management.index')->withSuccess(trans('app.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Review::destroy($id);

        return back()->withSuccess(trans('app.success_destroy'));
    }
}
