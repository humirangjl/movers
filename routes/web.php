<?php
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

/*
|------------------------------------------------------------------------------------
| Admin
|------------------------------------------------------------------------------------
*/
Route::group(['namespace' => 'Backend', 'prefix' => ADMIN, 'as' => ADMIN . '.', 'middleware'=>['auth', 'Role:10']], function () {
    Route::get('/', 'MainController@getIndex');
    Route::resource('users',              'UserController');
    Route::group(["prefix" => "content-management"], function(){
        Route::get('/remove-repeater-fields', 'ContentManagementController@getRemoveRepeaterFields');
        Route::get('/repeater-fields'       , 'ContentManagementController@getRepeaterFields');
    });
    Route::resource('company-management',   'CompanyManagementController');
    Route::resource('review-management',    'ReviewManagementController');
    Route::resource('content-management',   'ContentManagementController');
    Route::group(["prefix" => "settings"], function(){
        // ---------- Post Method ---------- //
        Route::post('default-cover',  'SettingsController@postDefaultCover');
        Route::post('default-avatar', 'SettingsController@postDefaultAvatar');
        Route::post('social',         'SettingsController@postSocial');
        Route::post('general',        'SettingsController@postGeneral');
        // ---------- Get Method ---------- //
        Route::get('/',               'SettingsController@getIndex');
    });
});
Route::group(['namespace' => 'Frontend'], function () {
    // POST REQUEST
    // GET REQUEST
    // Route::get('fixedDatabase',         'MainController@getFixedDatabase');
    Route::get('/',                     'MainController@getIndex');
    Route::get('find-mover',            'MainController@getFindMover');
    Route::get('rating-and-reports',    'MainController@getRatingAndReports');
    Route::group(["prefix" => "reviews"], function(){
        // ---------- Post Method ---------- //
        // ---------- Get Method ---------- //
        Route::get('/',                     'ReviewController@index');
        Route::get('{mover}/{review}',      'ReviewController@getReview');
    });
    Route::group(["prefix" => "movers"], function(){
        // ---------- Post Method ---------- //
        // ---------- Get Method ---------- //
        Route::get('{mover}',       'MainController@getMoversProfile');
    });
    Route::get('about-us',          'MainController@getAboutUs');
    Route::get('mover-login',       'MainController@getMoverLogin');
    Route::get('quote',             'MainController@getQuote');
    Route::get('write-a-review',    'MainController@getWriteAReview');
    Route::get('mover-registration' ,'MainController@getMoverregistration');
    Route::get('/auth/redirect/{provider}', 'SocialController@redirect');
    Route::get('/callback/{provider}', 'SocialController@callback');
    Route::get('/forgot-password','forgotController@forgot');
    Route::post('/forgot-password','forgotController@password');
    Route::get('/reset_password/{email}/{code}','forgotController@reset');
    Route::post('/reset_password/{email}/{code}','forgotController@resetPassword');
    Route::post('/request-quote','QuoteController@storequote');
});

Auth::routes();
