<?php

use App\User;
use Illuminate\Database\Seeder;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $data = [];

        for ($i = 1; $i <= 1 ; $i++) {
            array_push($data, [
                'name' => 'ADMIN',
                'email' => 'admin@gmail.com',
                'avatar' => 'favicon.png',
                'password' => bcrypt('12345'),
                'role'     => 10,
                'bio'      => $faker->realText(),
            ]);
        }

        User::insert($data);
    }
}
