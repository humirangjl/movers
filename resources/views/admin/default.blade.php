<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('backend/images/icons')}}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('backend/images/icons')}}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('backend/images/icons')}}/favicon-16x16.png">
    <link rel="manifest" href="{{asset('backend/images/icons')}}/site.webmanifest">
    <link rel="mask-icon" href="{{asset('backend/images/icons')}}/safari-pinned-tab.svg" color="#249abc">
    <meta name="msapplication-TileColor" content="#4fc4f1">
    <meta name="msapplication-TileImage" content="{{asset('backend/images/icons')}}/mstile-144x144.png">
    <meta name="theme-color" content="#50c1ef">
    <title>@yield('title'){{' ~ '.config('app.name').' Admin'}}</title>
    <!-- Styles -->
	<link href="{{ asset('backend/css/app.css') }}" rel="stylesheet">
	@yield('css')
</head>
<body class="app">
    @include('admin.partials.spinner')
    <div>
        <!-- #Left Sidebar ==================== -->
        @include('admin.partials.sidebar')
        <!-- #Main ============================ -->
        <div class="page-container">
            <!-- ### $Topbar ### -->
            @include('admin.partials.topbar')
            <!-- ### $App Screen Content ### -->
            <main class='main-content bgc-grey-100'>
                <div id='mainContent'>
                    <div class="container-fluid">
                        <h4 class="c-grey-900 mT-10 mB-30">@yield('page-header')</h4>
						@include('admin.partials.messages')
						@yield('content')
                    </div>
                </div>
            </main>
            <!-- ### $App Screen Footer ### -->
            <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600">
                <span>Copyright © 2018 Designed by
                    <a href="https://maverickheroes.com" target='_blank' title="Maverick Heroes">Maverick Heroes</a>. All rights reserved.</span>
            </footer>
        </div>
    </div>
    <script src="{{ asset('backend/js/app.js') }}"></script>
    @yield('js')
</body>
</html>
