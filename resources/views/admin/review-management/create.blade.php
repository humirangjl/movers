@extends('admin.default')
@section('title', 'Create Review')
@section('page-header')
	Review Management <small>{{ trans('app.add_new_item') }}</small>
@stop
@section('css')
<style>
    .d-flex .form-group {
        width: 100%;
    }
    .d-flex .form-group:first-child {
        margin-right: 10px;
    }
    input[type=radio] {
        position: absolute;
    }
    .radio label::after {
        top: 5px;
    }
</style>
@endsection
@section('content')
	{!! Form::open([
			'action' => ['Backend\ReviewManagementController@store'],
			'files' => true
		])
	!!}

		@include('admin.review-management.form')

		<button type="submit" class="btn btn-primary">{{ trans('app.add_button') }}</button>

	{!! Form::close() !!}

@stop
