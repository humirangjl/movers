@extends('admin.default')
@section('title', 'Edit Review')
@section('page-header')
	Review Management <small>{{ trans('app.update_item') }}</small>
@stop
@section('css')
<style>
    .d-flex .form-group {
        width: 100%;
    }
    .d-flex .form-group:first-child {
        margin-right: 10px;
    }
    input[type=radio] {
        position: absolute;
    }
    .radio label::after {
        top: 5px;
    }
</style>
@endsection
@section('content')
	{!! Form::model($item, [
			'action' => ['Backend\ReviewManagementController@update', $item->id],
			'method' => 'put',
			'files' => true
		])
    !!}
    @php
        // Customer Service
        $item->honesty          = $item->customer_service['honesty'];
        $item->knowledgeable    = $item->customer_service['knowledgeable'];
        $item->preparedness     = $item->customer_service['preparedness'];
        $item->overall_service  = $item->customer_service['overall_service'];

    @endphp
		@include('admin.review-management.form')

		<button type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>

	{!! Form::close() !!}

@stop
