<div class="form-group">
        <label for="company_id">Company</label>
        <select name="company_id" id="company_id" class="form-control selectpicker with-ajax" data-live-search="true">
            @if (isset($item))
            <option value="{{$item->company->id}}" data-subtext="{{$item->company->state}}" selected>
                {{$item->company->name}}
            </option>
            @endif
            @foreach ($companies as $key => $company)
            <option value="{{$company->id}}" data-subtext="{{$company->state}}">{{$company->name}}</option>
            @endforeach
        </select>
    </div>
