@php
    $states = [
        0                   => 'Choose Here',
        'Alabama'           => 'Alabama',
        'Alaska'            => 'Alaska',
        'Arizona'           => 'Arizona',
        'Arkansas'          => 'Arkansas',
        'California'        => 'California',
        'Colorado'          => 'Colorado',
        'Connecticut'       => 'Connecticut',
        'Delaware'          => 'Delaware',
        'Florida'           => 'Florida',
        'Georgia'           => 'Georgia',
        'Hawaii'            => 'Hawaii',
        'Idaho'             => 'Idaho',
        'Illinois'          => 'Illinois',
        'Indiana'           => 'Indiana',
        'Iowa'              => 'Iowa',
        'Kansas'            => 'Kansas',
        'Kentucky'          => 'Kentucky',
        'Louisiana'         => 'Louisiana',
        'Maine'             => 'Maine',
        'Maryland'          => 'Maryland',
        'Massachusetts'     => 'Massachusetts',
        'Michigan'          => 'Michigan',
        'Minnesota'         => 'Minnesota',
        'Mississippi'       => 'Mississippi',
        'Missouri'          => 'Missouri',
        'Montana'           => 'Montana',
        'Nebraska'          => 'Nebraska',
        'Nevada'            => 'Nevada',
        'New Hampshire'     => 'New Hampshire',
        'New Jersey'        => 'New Jersey',
        'New Mexico'        => 'New Mexico',
        'New York'          => 'New York',
        'North Carolina'    => 'North Carolina',
        'North Dakota'      => 'North Dakota',
        'Ohio'              => 'Ohio',
        'Oklahoma'          => 'Oklahoma',
        'Oregon'            => 'Oregon',
        'Pennsylvania'      => 'Pennsylvania',
        'Rhode Island'      => 'Rhode Island',
        'South Carolina'    => 'South Carolina',
        'South Dakota'      => 'South Dakota',
        'Tennessee'         => 'Tennessee',
        'Texas'             => 'Texas',
        'Utah'              => 'Utah',
        'Vermont'           => 'Vermont',
        'Virginia'          => 'Virginia',
        'Washington'        => 'Washington',
        'West Virginia'     => 'West Virginia',
        'Wisconsin'         => 'Wisconsin',
        'Wyoming'           => 'Wyoming',
    ];
    $rates = [
        '0.5'   => '0.5',
        '1'     => '1',
        '1.5'   => '1.5',
        '2'     => '2',
        '2.5'   => '2.5',
        '3'     => '3',
        '3.5'   => '3.5',
        '4'     => '4',
        '4.5'   => '4.5',
        '5'     => '5',
    ];
@endphp
<div class="row mB-40">
	<div class="col-sm-8">
		<div class="bgc-white p-20 bd">
            <div id="appendCompanies">
                @include('admin.review-management.append.companies')
            </div>
            <div class="d-flex">
                {!! Form::mySelect('rating', 'Rating', $rates, null, ['class' => 'form-control selectpicker']) !!}
                <div class="form-group">
                    <label for="">Recommended</label>
                    <div class="row">
                        <div class="col">{!! Form::myRadioButton('success', 'recommended', 'Yes', 'yes') !!}</div>
                        <div class="col">{!! Form::myRadioButton('danger', 'recommended', 'No', 'no') !!}</div>
                    </div>
                </div>
            </div>
            {!! Form::myInput('text', 'name', 'Name') !!}
            {!! Form::myInput('text', 'title', 'Title') !!}
            <div class="d-flex">
                {!! Form::myDate('date', 'Date', [], \Carbon\Carbon::now()) !!}
                {!! Form::mySelect('state', 'Move Within (Optional)', $states, null, ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) !!}
            </div>
            {!! Form::myTextArea('description', 'Description') !!}
            <label for="">Customer Service</label>
            <div class="container mt-2">
                <div class="d-flex">
                        {!! Form::mySelect('honesty', 'Honesty', $rates, null, ['class' => 'form-control selectpicker']) !!}
                        {!! Form::mySelect('knowledgeable', 'Knowledgeable', $rates, null, ['class' => 'form-control selectpicker']) !!}
                </div>
                <div class="d-flex">
                        {!! Form::mySelect('preparedness', 'Preparedness`', $rates, null, ['class' => 'form-control selectpicker']) !!}
                        {!! Form::mySelect('overall_service', 'Overall Service', $rates, null, ['class' => 'form-control selectpicker']) !!}
                </div>
            </div>
		</div>
	</div>
</div>
