@php
    $states = [
        'Alabama'           => 'Alabama',
        'Alaska'            => 'Alaska',
        'Arizona'           => 'Arizona',
        'Arkansas'          => 'Arkansas',
        'California'        => 'California',
        'Colorado'          => 'Colorado',
        'Connecticut'       => 'Connecticut',
        'Delaware'          => 'Delaware',
        'Florida'           => 'Florida',
        'Georgia'           => 'Georgia',
        'Hawaii'            => 'Hawaii',
        'Idaho'             => 'Idaho',
        'Illinois'          => 'Illinois',
        'Indiana'           => 'Indiana',
        'Iowa'              => 'Iowa',
        'Kansas'            => 'Kansas',
        'Kentucky'          => 'Kentucky',
        'Louisiana'         => 'Louisiana',
        'Maine'             => 'Maine',
        'Maryland'          => 'Maryland',
        'Massachusetts'     => 'Massachusetts',
        'Michigan'          => 'Michigan',
        'Minnesota'         => 'Minnesota',
        'Mississippi'       => 'Mississippi',
        'Missouri'          => 'Missouri',
        'Montana'           => 'Montana',
        'Nebraska'          => 'Nebraska',
        'Nevada'            => 'Nevada',
        'New Hampshire'     => 'New Hampshire',
        'New Jersey'        => 'New Jersey',
        'New Mexico'        => 'New Mexico',
        'New York'          => 'New York',
        'North Carolina'    => 'North Carolina',
        'North Dakota'      => 'North Dakota',
        'Ohio'              => 'Ohio',
        'Oklahoma'          => 'Oklahoma',
        'Oregon'            => 'Oregon',
        'Pennsylvania'      => 'Pennsylvania',
        'Rhode Island'      => 'Rhode Island',
        'South Carolina'    => 'South Carolina',
        'South Dakota'      => 'South Dakota',
        'Tennessee'         => 'Tennessee',
        'Texas'             => 'Texas',
        'Utah'              => 'Utah',
        'Vermont'           => 'Vermont',
        'Virginia'          => 'Virginia',
        'Washington'        => 'Washington',
        'West Virginia'     => 'West Virginia',
        'Wisconsin'         => 'Wisconsin',
        'Wyoming'           => 'Wyoming',
    ];
@endphp
<div class="row mB-40">
	<div class="col-sm-8">
		<div class="bgc-white p-20 bd">
			{!! Form::myInput('text', 'name', 'Name') !!}
			{!! Form::myInput('text', 'website', 'Website URL', ['placeholder' => 'http:// or https://']) !!}
            <div class="d-flex">
                    {!! Form::myInput('text', 'street_address', 'Street Address (Optional)') !!}
                    {!! Form::myInput('text', 'city', 'City') !!}
            </div>
            <div class="row">
                <div class="col">
                    {!! Form::myInput('text', 'state', 'State') !!}
                </div>
                <div class="col">
                    {!! Form::myInput('text', 'zip_code', 'Zip Code (Optional)') !!}
                </div>
                <div class="col">
                    {!! Form::myInput('text', 'country', 'Country') !!}
                </div>
            </div>
            {!! Form::mySelect('operation[]', 'State of Operation', $states, null, ['class' => 'form-control selectpicker', 'multiple' => 'multiple', 'data-live-search' => 'true', 'data-actions-box' => 'true']) !!}
            {!! Form::myTextArea('description', 'Description (Optional)') !!}
            {!! Form::myInput('email', 'email', 'Email (Optional)') !!}
            {!! Form::myInput('text', 'phone_number', 'Phone Number (Optional)')!!}
            <div class="d-flex">
                    {!! Form::myInput('text', 'facebook', 'Facebook (Optional)') !!}
                    {!! Form::myInput('text', 'twitter', 'Twitter (Optional)') !!}
            </div>
            <div class="d-flex">
                    {!! Form::myInput('text', 'instagram', 'Instagram (Optional)') !!}
                    {!! Form::myInput('text', 'whatsapp', 'Whatsapp (Optional)') !!}
            </div>
            <div class="form-group">
                <label for="logo">Logo</label>
                <div id="image-preview" style="background: url({{isset($item) && $item->logo != '' ? URL($item->logo) : ''}});background-size: contain;background-repeat: no-repeat;background-position: center;">
                    <label for="image-upload" id="image-label">Choose Logo</label>
                    <input type="file" name="logo" id="image-upload" />
                </div>
            </div>
		</div>
	</div>
</div>
