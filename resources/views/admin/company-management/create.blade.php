@extends('admin.default')
@section('title', 'Create Company')
@section('page-header')
	Company Management <small>{{ trans('app.add_new_item') }}</small>
@stop
@section('css')
<style>
    .d-flex .form-group {
        width: 100%;
    }
    .d-flex .form-group:first-child {
        margin-right: 10px;
    }
</style>
@endsection
@section('content')
	{!! Form::open([
			'action' => ['Backend\CompanyManagementController@store'],
			'files' => true
		])
	!!}

		@include('admin.company-management.form')

		<button type="submit" class="btn btn-primary">{{ trans('app.add_button') }}</button>

	{!! Form::close() !!}

@stop
