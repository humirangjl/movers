@extends('admin.default')
@section('title', 'Edit Company')
@section('page-header')
	Company Management <small>{{ trans('app.update_item') }}</small>
@stop
@section('css')
<style>
    .d-flex .form-group {
        width: 100%;
    }
    .d-flex .form-group:first-child {
        margin-right: 10px;
    }
</style>
@endsection
@section('content')
	{!! Form::model($item, [
			'action' => ['Backend\CompanyManagementController@update', $item->id],
			'method' => 'put',
			'files' => true
		])
    !!}
    @php
        // Address
        $item->street_address   = $item->address;

        // Social Medias
        $item->facebook = $item->social_media['facebook'];
        $item->twitter = $item->social_media['twitter'];
        $item->instagram = $item->social_media['instagram'];
        $item->whatsapp = $item->social_media['whatsapp'];
    @endphp
		@include('admin.company-management.form')

		<button type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>

	{!! Form::close() !!}

@stop
