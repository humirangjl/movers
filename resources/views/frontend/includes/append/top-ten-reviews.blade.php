@foreach ($reviews as $key => $review)
<div class="review-item">
    <a href="{{url('movers/'.$review->company->slug)}}" class="company-details">
        <div class="logo-holder">
            <img src="{!!asset($review->company->logo)!!}" alt="">
        </div>
        <div class="company-info">
            <p>{{$review->company->state}} based moving company:</p>
            <h3>{{$review->company->name}}</h3>
            <ul class="star-rate list-inline">
                <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                <li class="list-inline-item">Reviewed {{$review->company->reviews->count()}} times</li>
            </ul>
        </div>
    </a>
    <div class="latest-review">
        <div class="title-reviewer">
            <img src="{{asset('frontend')}}/img/reviewer-image.png" alt="">
            <div class="upper">
                <h2>{{$review->title}}</h2>
                <p>Reviewed {{\Carbon\Carbon::parse($review->date)->diffForHumans()}} by <strong>{{$review->name}}</strong>{{$review->state?', moved within '.$review->state:''}}</p>
            </div>
        </div>
        <div class="lower">
            <p>{{str_limit($review->description, 400)}}</p>
            <label class="recommended"><i class="fa fa-{{$review->recommended == 'yes'?'check':'times'}}-circle"></i>Recommended</label>
            <a href="{{url('reviews/'.$review->company->slug.'/'.$review->slug)}}" class="more-review btnStyle-1" title="">View Full Review</a>
        </div>
    </div>
</div>
@endforeach
@if ($reviews->currentPage() != $reviews->lastPage())
<div class="text-center">
    <a class="load-more" href="#" title="" data-page="{{$reviews->currentPage()}}">View More Reviews</a>
</div>
@endif
