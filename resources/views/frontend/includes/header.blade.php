<header class="navigation">
    <div class="container relative">
        <nav class="navbar navbar-expand-lg navbar-dark">
            <a class="navbar-brand" href="{{url('/')}}">
                <img src="{{asset('uploads/logo/'.$configuration->logo)}}">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse pull-right" id="navbarCollapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item {{Request::is('/')?'active':''}}">
                        <a class="nav-link" href="{{url('/')}}">Home</a>
                    </li>
                    <li class="nav-item {{Request::is('find-mover')?'active':''}}">
                        <a class="nav-link" href="{{url('find-mover')}}">Find  Mover</a>
                    </li>
                    <li class="nav-item {{Request::is('rating-and-reports')?'active':''}}">
                        <a class="nav-link" href="{{url('rating-and-reports')}}">Rating & Reports</a>
                    </li>
                    <li class="nav-item {{Request::is('reviews')?'active':''}}">
                        <a class="nav-link" href="{{url('reviews')}}">Reviews</a>
                    </li>
                    <li class="nav-item {{Request::is('about-us')?'active':''}}">
                        <a class="nav-link" href="{{url('about-us')}}">About Us</a>
                    </li>
                    @if(Route::has('login'))
                    @auth
                     <li class="nav-item">
                        <a class="nav-link" href="{{route('logout')}}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    		    @csrf
                           </form>
                    </li>
                    @else
                     <li class="nav-item {{Request::is('mover-login')?'active':''}}">
                        <a class="nav-link" href="{{url('mover-login')}}">Mover Login</a>
                    </li>
                    @endauth
                    @endif
                   
                    <li class="nav-item mobile-only getQoute-nav">
                        <a class="nav-link" href="{{url('quote')}}">Get Quote</a>
                    </li>
                </ul>
            </div>
            <a href="{{url('quote')}}" class="getQoute-header" class="btn btn-info" title="">Get Quote <i class="fa fa-angle-right"></i></a>
        </nav>
    </div>
</header>
