<footer>
    <div class="recent-footer text-center">
        <img src="{{asset('frontend')}}/img/recently-bg.png" alt="">
        <div class="container">
            <h2>Moved Recently?</h2>
            <p>Share your moving experience with the world and help thousands select better movers.</p>
            <a href="{{url('write-a-review')}}" class="btnStyle-1" title="">Write A Review</a>
        </div>
    </div>
    <div class="copy-links text-center">
        <div class="container">
            <p>{!!$configuration->copyright!!}</p>
            <ul class="list-inline">
                <li class="list-inline-item"><a href="#" title="">Home</a></li>
                <li class="list-inline-item"><a href="#" title="">Find  Company</a></li>
                <li class="list-inline-item"><a href="#" title="">Rating & Reports</a></li>
                <li class="list-inline-item"><a href="#" title="">Reviews</a></li>
                <li class="list-inline-item"><a href="#" title="">About Us</a></li>
                <li class="list-inline-item"><a href="#" title="">Company Login</a></li>
                <li class="list-inline-item"><a href="#" title="">Contact</a></li>
            </ul>
        </div>
    </div>
</footer>
