@extends('frontend.layout')
@section('title','Mover Profile')
@section('css')
@stop
@section('content')
<main role="main" class="contents">
    <section class="company-who padder">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 col-lg-9">
                    <div class="who-is">
                        <div class="logo-holder">
                            <img src="{{asset('frontend')}}/img/logo-1.png" alt="">
                        </div>
                        <div class="info-holder">
                            <p>Florida based moving company:</p>
                            <h2>Northern Moving Systems</h2>
                            <div class="track clearfix">
                                <ul class="star-rate list-inline">
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                </ul>
                                <div class="top-rated">Top Rated</div>
                            </div>
                            <label>176 Reviews <span>|</span> 4.1 out of 5 rating</label>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3">
                    <a class="waeyo-1" href="#" title="">Visit Website</a>
                    <a class="waeyo-2" href="{{url('write-a-review?mover=mover-name')}}" title="">Write Review</a>
                </div>
            </div>
        </div>
    </section>
    <section class="this-company-reviews">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 col-lg-9">
                    <div class="review-graph">
                        <h2>Customer Review <span>176 Reviews</span></h2>
                        <ul class="list-unstyled">
                            <li class="percent s5">
                                <div>5 star</div>
                                <div><div style="width: 75%;"></div></div>
                                <div>78%</div>
                            </li>
                            <li class="percent s4">
                                <div>4 star</div>
                                <div><div style="width: 11%;"></div></div>
                                <div>11%</div>
                            </li>
                            <li class="percent s3">
                                <div>3 star</div>
                                <div><div style="width: 8%;"></div></div>
                                <div>8%</div>
                            </li>
                            <li class="percent s2">
                                <div>2 star</div>
                                <div><div style="width: 5%;"></div></div>
                                <div>5%</div>
                            </li>
                            <li class="percent s1">
                                <div>1 star</div>
                                <div><div style="width: 1%;"></div></div>
                                <div>1%</div>
                            </li>
                        </ul>
                    </div>
                    <div class="reviews-wrapper">
                        <div class="item-wrapper">
                            <div class="title-reviewer">
                                <img class="reviewer" src="{{asset('frontend')}}/img/reviewer-image.png" alt="">
                                <div class="upper">
                                    <h2>“ Good work! I recommend this moving company! ”</h2>
                                    <p>Reviewed 1 hour ago by <strong>Jojo Binay</strong>, moved within Texas</p>
                                </div>
                                <ul class="star-rate list-inline">
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                </ul>
                            </div>
                            <div class="content-reviewer">
                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus.</p>
                                <ul class="list-inline recommended">
                                    <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Verified User</label></li>
                                    <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Recommended</label></li>
                                </ul>
                                <a href="{{url('reviews/mover-name/review-title')}}" class="more-review btnStyle-1" title="">View Full Review</a>
                            </div>
                        </div>
                        <div class="item-wrapper">
                            <div class="title-reviewer">
                                <img class="reviewer" src="{{asset('frontend')}}/img/reviewer-image.png" alt="">
                                <div class="upper">
                                    <h2>“Secure and reliable!”</h2>
                                    <p>Reviewed 1 hour ago by <strong>Jojo Binay</strong>, moved within Texas</p>
                                </div>
                                <ul class="star-rate list-inline">
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                </ul>
                            </div>
                            <div class="content-reviewer">
                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus.</p>
                                <ul class="list-inline recommended">
                                    <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Verified User</label></li>
                                    <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Recommended</label></li>
                                </ul>
                                <a href="{{url('reviews/mover-name/review-title')}}" class="more-review btnStyle-1" title="">View Full Review</a>
                            </div>
                        </div>
                        <div class="item-wrapper">
                            <div class="title-reviewer">
                                <img class="reviewer" src="{{asset('frontend')}}/img/reviewer-image.png" alt="">
                                <div class="upper">
                                    <h2>“Amazing job, great service...”</h2>
                                    <p>Reviewed 1 hour ago by <strong>Jojo Binay</strong>, moved within Texas</p>
                                </div>
                                <ul class="star-rate list-inline">
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                </ul>
                            </div>
                            <div class="content-reviewer">
                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus.</p>
                                <ul class="list-inline recommended">
                                    <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Verified User</label></li>
                                    <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Recommended</label></li>
                                </ul>
                                <a href="{{url('reviews/mover-name/review-title')}}" class="more-review btnStyle-1" title="">View Full Review</a>
                            </div>
                        </div>
                        <div class="item-wrapper">
                            <div class="title-reviewer">
                                <img class="reviewer" src="{{asset('frontend')}}/img/reviewer-image.png" alt="">
                                <div class="upper">
                                    <h2>“Secure and reliable!”</h2>
                                    <p>Reviewed 1 hour ago by <strong>Jojo Binay</strong>, moved within Texas</p>
                                </div>
                                <ul class="star-rate list-inline">
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                </ul>
                            </div>
                            <div class="content-reviewer">
                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus.</p>
                                <ul class="list-inline recommended">
                                    <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Verified User</label></li>
                                    <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Recommended</label></li>
                                </ul>
                                <a href="{{url('reviews/mover-name/review-title')}}" class="more-review btnStyle-1" title="">View Full Review</a>
                            </div>
                        </div>
                        <div class="item-wrapper">
                            <div class="title-reviewer">
                                <img class="reviewer" src="{{asset('frontend')}}/img/reviewer-image.png" alt="">
                                <div class="upper">
                                    <h2>“Amazing job, great service...”</h2>
                                    <p>Reviewed 1 hour ago by <strong>Jojo Binay</strong>, moved within Texas</p>
                                </div>
                                <ul class="star-rate list-inline">
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                    <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                </ul>
                            </div>
                            <div class="content-reviewer">
                                <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus.</p>
                                <ul class="list-inline recommended">
                                    <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Verified User</label></li>
                                    <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Recommended</label></li>
                                </ul>
                                <a href="{{url('reviews/mover-name/review-title')}}" class="more-review btnStyle-1" title="">View Full Review</a>
                            </div>
                        </div>
                        <div class="text-right">
                            <ul class="list-inline-item">
                                <li class="list-inline-item paging">Page</li>
                                <li class="list-inline-item"><a class="paging active" href="#" title="">1</a></li>
                                <li class="list-inline-item"><a class="paging" href="#" title="">2</a></li>
                                <li class="list-inline-item"><a class="paging" href="#" title="">3</a></li>
                                <li class="list-inline-item"><a class="paging" href="#" title="">Next</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 review-area">
                    <div class="sidebar">
                        <div class="about-contact">
                            <h2 class="side-headers">About</h2>
                            <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus.<br><br>
                            Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus.</p>
                            <h2 class="side-headers">Contact</h2>
                            <ul class="list-unstyled contact-here">
                                <li><i class="fa fa-envelope"></i><a href="mailto:info@northernmovingsystems.com" title="">info@northernmovingsystems.com</a></li>
                                <li><i class="fa fa-phone"></i><a href="tel:8878859226" title="">(877) 885-9226</a></li>
                            </ul>
                            <ul class="list-inline social-links">
                                <li class="list-inline-item"><a href="#" title="" class="fa fa-facebook"></a></li>
                                <li class="list-inline-item"><a href="#" title="" class="fa fa-whatsapp"></a></li>
                                <li class="list-inline-item"><a href="#" title="" class="fa fa-twitter"></a></li>
                                <li class="list-inline-item"><a href="#" title="" class="fa fa-instagram"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@stop
@section('js')
<script type="text/javascript">

    $(document).ready(function() {
        $('select:not(.not-nice)').niceSelect();
    });

</script>
@stop
