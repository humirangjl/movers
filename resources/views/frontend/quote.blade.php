@extends('frontend.layout')
@section('title','Quote')
@section('css')
@stop
@section('content')

<main role="main" class="contents">
    <section class="qoute-wrapper padder">
        <div class="container">
            <div class="section-title text-center">
                <h2>GET YOUR QUOTE</h2>
            </div>
             @include('frontend.includes.message')
            <form class="form-wrapper" action="{{URL('request-quote')}}" method="post" accept-charset="utf-8">
                @csrf
                <h4><span>1</span> Where Will You Move?</h4>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="name">Enter Zip Code</label>
                            <input type="text" class="form-control" name="zip_code" placeholder="Enter Zip Code">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="name">Select State</label>
                            <input type="hidden" name="country" id="countryId" value="US"/>
                            <select name="state" class="form-control states order-alpha" id="stateId">
                                <option value="">Select State</option>     
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="name">Select City</label>
                           <select name="city" class="form-control cities order-alpha" id="cityId">
                            <option value="">Select City</option>
                        </select>
                        </div>
                    </div>
                </div>
                <h4><span>2</span> What are you moving?</h4>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="name">Moving Size</label>
                            <select class="form-control" name="moving_size">
                                <option selected hidden>Please Select</option>
                                <option value="s">Sample 1</option>
                                <option value="s">Sample 1</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="name">Additional moving</label>
                            <input type="text" class="form-control" name="additional_moving" placeholder="Enter here">
                        </div>
                    </div>
                </div>
                <h4><span>3</span> When Will You Move?</h4>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group withIcon">
                            <label for="name">Moving Date</label>
                            <i class="fa fa-calendar"></i>
                            <input class="form-control" type="date" id="datepicker" name="movingdate" placeholder="Pick a date">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="name">Enter Name</label>
                            <input class="form-control" type="text" name="fullname" placeholder="Enter full name">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="name">Enter E-mail Address</label>
                            <input class="form-control" type="email" name="email" placeholder="Enter e-mail address">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="name">Enter Phone Number</label>
                            <input class="form-control" type="text" name="phone_number" onkeypress="return isNumberKey(event)"   placeholder="Enter phone number">
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btnStyle-1">Submit Quote</button>
                </div>
            </form>
        </div>
    </section>
</main>
@stop
@section('js')
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<script src="//geodata.solutions/includes/statecity.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $('select:not(.not-nice)').niceSelect();
    });

    $( function() {
        $( "#datepicker" ).datepicker({
            minDate:0,
        });
    });
     
      function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
</script>
@stop
