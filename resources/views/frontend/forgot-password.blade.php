@extends('frontend.layout')
@section('title','Mover Login')
@section('css')
@stop
@section('content')
<main role="main" class="contents">
    <section class="login-wrapper padder">
        <div class="container">
            <div class="section-title text-center">
                <h2></h2>
            </div>
            <div class="form-wrapper">
                <div class="site-log">
                    <h2>Forgot Password</h2>
                    @include('frontend.includes.message')
                <form action="{{url('forgot-password')}}" method="post" accept-charset="utf-8">
                @csrf
            
							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							
								<input id="email" type="email" class="form-control" name="email" placholder="Enter Email" required autofocus>

                            @if ($errors->has('email'))
                                <span class="form-text text-danger">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
							</div>
							<div class="form-group">
								<input type="submit" name="" value="Continue" class="btnStyle-1">
							</div>
						</form>
                </div>
            </div>
        </div>
    </section>
</main>
@stop
@section('js')
<script type="text/javascript">

    $(document).ready(function() {
        $('select:not(.not-nice)').niceSelect();
    });

</script>
@stop
