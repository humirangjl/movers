@extends('frontend.layout')
@section('title','Write A Review')
@section('css')
@stop
@section('content')
<main role="main" class="contents">
    <section class="contact-wrapper write-review padder">
        <div class="container">
            <div class="section-title text-center">
                <h2>create your review!</h2>
            </div>
            <form class="form-wrapper" action="" method="get" accept-charset="utf-8">
                <div class="form-group">
                    <label for="name">Please select a company to review</label>
                    <select name="company" class="form-control">
                        <option disabled hidden>Please select a company to review</option>
                        <option value="">Movers 1</option>
                        <option value="">Movers 2</option>
                    </select>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="email">Title of your review</label>
                            <input type="text" name="" class="form-control" placeholder="Enter Title">
                        </div>
                        <div class="form-group">
                            <label for="message">Write your review</label>
                            <textarea name="message" placeholder="Write your review" class="form-control"></textarea>
                        </div>
                        <div class="form-group checkbox">
                            <label><input type="checkbox" name="">I agree to the customer reviews <a href="#" title="">Terms and Conditions</a></label>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="email">Rating</label>
                            <div class="my-rating"></div>
                        </div>
                        <div class="form-group mb-3">
                            <label for="email">Customer Service</label>
                            <div class="form-inline">
                                <label class="star-holder">Honesty</label>
                                <div class="my-rating"></div>
                            </div>
                            <div class="form-inline">
                                <label class="star-holder">Knowledgeable</label>
                                <div class="my-rating"></div>
                            </div>
                            <div class="form-inline">
                                <label class="star-holder">Preparedness</label>
                                <div class="my-rating"></div>
                            </div>
                            <div class="form-inline">
                                <label class="star-holder">Overall Service</label>
                                <div class="my-rating"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center">
                    <input type="submit" name="" class="btnStyle-1">
                </div>
            </form>
        </div>
    </section>
</main>
@stop
@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $('select:not(.not-nice)').niceSelect();

        $(".my-rating").starRating({
            starSize: 23,
            disableAfterRate: false,
            maxRating: 5,
            callback: function(currentRating, $el){
                // make a server call here
            }
        });
    });
</script>
@stop
