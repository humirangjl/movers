@extends('frontend.layout')
@section('title','Mover Login')
@section('css')
@stop
@section('content')
<main role="main" class="contents">
    <section class="login-wrapper padder">
        <div class="container">
            <div class="form-wrapper ">
                 <h2  class="text-center">Sign Up</h2>
				 <hr>
              <form action="{{ route('register') }}" method="post" accept-charset="utf-8">
			  @csrf
							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label>Name</label>
								  <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
									@if ($errors->has('name'))
										<span class="form-text text-danger">
											<small>{{ $errors->first('name') }}</small>
										</span>
									@endif
							</div>
							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<label>Email</label>
								 <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

								@if ($errors->has('email'))
									<span class="form-text text-danger">
										<small>{{ $errors->first('email') }}</small>
									</span>
								@endif
							</div>
							<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<label>Password</label>
								  <input id="password" type="password" class="form-control" name="password" required>

								@if ($errors->has('password'))
									<span class="form-text text-danger">
										<small>{{ $errors->first('password') }}</small>
									</span>
								@endif
							</div>
							<div class="form-group">
								<label>Confirm Password</label>
							  <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required>
							</div>
							<div class="form-group{{ $errors->has('terms') ? ' has-error' : '' }}">
								<label><input type="checkbox" name="terms" value="1">By creating an account, you agree to our <a href="#" title="">Terms of Conditions</a>.</label>
							    @if ($errors->has('terms'))
									<span class="form-text text-danger">
										<small>{{ $errors->first('terms') }}</small>
									</span>
								@endif    
							</div>
							<div class="form-group text-center">
								<button type="submit" name=""  class="btnStyle-1">Sign Up</button>
							</div>
						</form>
            </div>
        </div>
    </section>
</main>
@stop
@section('js')
<script type="text/javascript">

    $(document).ready(function() {
        $('select:not(.not-nice)').niceSelect();
    });

</script>
@stop
