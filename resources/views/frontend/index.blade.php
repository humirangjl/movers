@extends('frontend.layout')
@section('title','Home')
@section('css')
<style>
    .review-area .reviews-wrapper .review-item .latest-review .lower .recommended i.fa-times-circle {
        color: #d75a4a;
    }
</style>
@stop
@section('content')
<main role="main" class="contents">
    <section class="home-banner banner">
        <img class="banner-img" src="{{asset('frontend')}}/img/home-banner.png" alt="">
        <div class="text-wrapper middle-div">
            <h2>LET US MOVE YOUR DREAM.</h2>
            <a href="{{url('write-a-review')}}" class="btnStyle-1" title="">Write A Review</a>
        </div>
    </section>
    <section class="direct-concise padder">
        <div class="container">
            <div class="content-holder">
                <div class="row">
                    <!-- <div class="col-lg-2">
                        <img src="{{asset('frontend')}}/img/qoute-box.png" alt="">
                    </div> -->
                    <div class="col-lg-12">
                        <h2>THE GUIDE TO THE BEST MOVERS.</h2>
                        <p>Welcome to Mover Reviews Direct. There are thousands of moving companies to choose from. Confused? That's why we are here.</p>
                    </div>
                    <div class="icon-holder">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="image-holder">
                                    <img src="{{asset('frontend')}}/img/icons/findmovers.png" alt="">
                                </div>
                                <h4>Find mover</h4>
                            </div>
                            <div class="col-md-4">
                                <div class="image-holder">
                                    <img src="{{asset('frontend')}}/img/icons/checkreviews.png" alt="">
                                </div>
                                <h4>check reviews</h4>
                            </div>
                            <div class="col-md-4">
                                <div class="image-holder">
                                    <img src="{{asset('frontend')}}/img/icons/moveyourdream.png" alt="">
                                </div>
                                <h4>move your dream</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="search-wrapper">
        <div class="container">
            <form class="qoute-holder">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            <select name="year" class="form-control">
                                <option selected>Past 2 Years</option>
                                <option >Past 1 Year</option>
                                <option >Past 6 Months</option>
                                <option >Past 3 Months</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <select name="rating" class="form-control">
                                <option selected>5 Star Rating</option>
                                <option >4 Star Rating</option>
                                <option >3 Star Rating</option>
                                <option >2 Star Rating</option>
                                <option >1 Star Rating</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <input type="text" class="form-control" name="" placeholder="Search Mover Reviews">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <input type="text" class="form-control" name="" placeholder="Search a moving question">
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <input type="submit" class="btnStyle-1" name="" value="Search">
                        </div>
                    </div>
                </div>
            </form>
            <div class="show-result">The 10 Latest Moving Company Reviews:</div>
            <div class="review-area">
                <div class="row">
                    <div class="col-xl-8 col-lg-9">
                        <div class="reviews-wrapper">
                            @include('frontend.includes.append.top-ten-reviews')
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-3">
                        <div class="sidebar">
                            <div class="nearby-movers">
                                <h2 class="side-headers">Find Movers Nearby</h2>
                                <form action="" method="get" accept-charset="utf-8">
                                    <input type="text" name="" class="form-control" placeholder="Enter zipcode">
                                    <i class="fa fa-search"></i>
                                </form>
                            </div>
                            <div class="state-movers">
                                <h2 class="side-headers">Find Movers in Your State</h2>
                                <ul class="list-unstyled">
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                            <div class="top-movers">
                                <h2 class="side-headers">Most Search Movers</h2>
                                <ul class="list-unstyled">
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@stop
@section('js')
<script type="text/javascript">
    // var timezone_offset_minutes = new Date().getTimezoneOffset();
    // timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;

    $(document).ready(function() {
        $('select:not(.not-nice)').niceSelect();
        // $.ajax({
        //     type: "get",
        //     data: {
        //         offset: timezone_offset_minutes,
        //     },
        //     dataType: "json",
        //     success: function (response) {

        //     }
        // });
        // return false;
    });

    $(document).on('click', '.load-more', function(e){
        $.ajax({
            type: "get",
            data: {
                page: $(this).data('page') + 1,
            },
            dataType: "dataType",
            success: function (response) {

            }
        });
        return false;
    })
</script>
@stop
