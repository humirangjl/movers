@extends('frontend.layout')
@section('title','Rating & Reports')
@section('css')
@stop
@section('content')
<main role="main" class="contents">
    <section class="inner-banner banner">
        <img class="banner-img" src="{{asset('frontend')}}/img/rating-banner.png" alt="">
        <div class="text-wrapper middle-div">
            <h2>(WE’LL HELP) YOU KNOW WHAT’S BEST FOR YOU.</h2>
            <a href="{{url('write-a-review')}}" class="btnStyle-1" title="">Write A Review</a>
        </div>
    </section>
    <section class="direct-concise padder">
        <div class="container">
            <div class="content-holder">
                <h2>Ratings & Reports</h2>
                <p>Find top rated movers here. On this page you will find a collection of reports, reviews and ratings from moving customers. This collection produces a score that will assist you in finding the best (or the worst) movers. Knowing which companies and services to avoid allows you to focus on the companies that truly deserve your business. We take your ratings and review seriously and we continuously update reviews as they are submitted, so you are getting the information as soon as we receive it. We are merely moderators, you determine the content. In addition, your reviews help us keep our vital service alive.</p>
                <div class="icon-holder">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="image-holder">
                                <img src="{{asset('frontend')}}/img/icons/search.png" alt="">
                            </div>
                            <h4>search</h4>
                        </div>
                        <div class="col-md-4">
                            <div class="image-holder">
                                <img src="{{asset('frontend')}}/img/icons/assess.png" alt="">
                            </div>
                            <h4>assess</h4>
                        </div>
                        <div class="col-md-4">
                            <div class="image-holder">
                                <img src="{{asset('frontend')}}/img/icons/decide.png" alt="">
                            </div>
                            <h4>decide</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="search-wrapper">
        <div class="container">
            <form class="qoute-holder">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            <select name="year" class="form-control">
                                <option selected>Sort By: Date</option>
                                <option >Sort By: Rating</option>
                                <option >Sort By: Reviews</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <select name="rating" class="form-control">
                                <option selected>5 Star Rating</option>
                                <option >4 Star Rating</option>
                                <option >3 Star Rating</option>
                                <option >2 Star Rating</option>
                                <option >1 Star Rating</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <input type="text" class="form-control" name="" placeholder="Search Reviews">
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <input type="submit" class="btnStyle-1" name="" value="Search">
                        </div>
                    </div>
                </div>
            </form>
            <div class="show-result">Ratings & Reports - Showing 3 of 3</div>
            <div class="review-area">
                <div class="row">
                    <div class="col-xl-8 col-lg-9">
                        <div class="reviews-report-changes">
                            <div class="review-item">
                                <a href="{{url('movers/mover-name')}}" class="company-details">
                                    <div class="logo-holder">
                                        <img src="{{asset('frontend')}}/img/logo-1.png" alt="">
                                        <p>Florida based moving company:</p>
                                        <h3>Northern Moving Systems</h3>
                                    </div>
                                    <div class="company-info">
                                        <ul class="star-rate list-inline">
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                            <li class="list-inline-item"><span>Top Rated</span> | Reviewed 176 times</li>
                                        </ul>
                                        <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec</p>
                                    </div>
                                </a>
                            </div>
                            <div class="review-item">
                                <a href="{{url('movers/mover-name')}}" class="company-details">
                                    <div class="logo-holder">
                                        <img src="{{asset('frontend')}}/img/Agilityvanlines.png" alt="">
                                        <p>Florida based moving company:</p>
                                        <h3>Agility Van Lines</h3>
                                    </div>
                                    <div class="company-info">
                                        <ul class="star-rate list-inline">
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                            <li class="list-inline-item"><span>Top Rated</span> | Reviewed 176 times</li>
                                        </ul>
                                        <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec</p>
                                    </div>
                                </a>
                            </div>
                            <div class="review-item">
                                <a href="{{url('movers/mover-name')}}" class="company-details">
                                    <div class="logo-holder">
                                        <img src="{{asset('frontend')}}/img/logo-3.png" alt="">
                                        <p>Florida based moving company:</p>
                                        <h3>Bekins</h3>
                                    </div>
                                    <div class="company-info">
                                        <ul class="star-rate list-inline">
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                            <li class="list-inline-item"><span>Top Rated</span> | Reviewed 176 times</li>
                                        </ul>
                                        <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec</p>
                                    </div>
                                </a>
                            </div>
                            <div class="text-right">
                                <a class="paging" href="#" title="">Page 1</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-3">
                        <div class="sidebar">
                            <div class="state-movers">
                                <h2 class="side-headers">Find Movers in Your State</h2>
                                <ul class="list-unstyled">
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                            <div class="top-movers">
                                <h2 class="side-headers">Most Search Movers</h2>
                                <ul class="list-unstyled">
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@stop
@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $('select:not(.not-nice)').niceSelect();
    });
</script>
@stop
