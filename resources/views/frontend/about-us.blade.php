@extends('frontend.layout')
@section('title','About Us')
@section('css')
@stop
@section('content')
<main role="main" class="contents">
    <section class="inner-banner banner">
        <img class="banner-img" src="{{asset('frontend')}}/img/about-banner.png" alt="">
        <div class="text-wrapper middle-div">
            <h2>REAL PEOPLE, REAL MOVES, REAL REVIEWS.</h2>
            <a href="{{url('write-a-review')}}" class="btnStyle-1" title="">Write A Review</a>
        </div>
    </section>
    <section class="about-section-1 padder">
        <div class="container">
            <h2>About the Moving Process</h2>
            <p>We are dedicated to helping customers find the right company by providing an extensive list of movers with reviews. Our goal is to create a platform for customers to share their moving experiences so that others may be aware of the benefits and the problems of moving. We’d like to thank you for going to our website just to share your valuable experiences here in Mover Reviews Direct. Let’s help one another, across the internet, in choosing the right moving company for you. Just like what our tagline says, make a well-informed decision with real people, real moves, and real reviews.</p>
        </div>
    </section>
    <section class="about-section-2 padder">
        <div class="container">
            <div class="section-title text-center">
                <h2>how we PERFORM</h2>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-6">
                    <div class="perform">
                        <h2>Harvest</h2>
                        <div class="image-holder">
                            <img src="{{asset('frontend')}}/img/perform-1.png" alt="">
                        </div>
                        <p>Glad you could stop by. We want to help you connect with the moving company that is right for you. Each piece of honest feedback shows you the good, the bad, the funny, and the ugly. Now, you need to be attentive to what customers have to say because whatever information they provide can help you make your decision. So please take the time you need to read so when you are ready to move, you can now make a decision and take that first step of turning your dreams into a reality.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="perform">
                        <h2>Sift</h2>
                        <div class="image-holder">
                            <img src="{{asset('frontend')}}/img/perform-2.png" alt="">
                        </div>
                        <p>Everyday, thousands of customers write reviews with strong emotions. Whether good or bad, these are still genuine reviews. Every comments, ratings, and reports that you need to know about moving companies can be found here. The solution to all that? Choose a company, make a thorough inspection, read reviews, and decide for yourself if the company fits the bill but please don’t forget to share it here.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="perform">
                        <h2>Take to Market</h2>
                        <div class="image-holder">
                            <img src="{{asset('frontend')}}/img/perform-3.png" alt="">
                        </div>
                        <p>Read the reviews carefully and look for patterns. Every moving company has a bad move or a bad customer, and if they are very unlucky, both at one time. Patterns, however, stand out. A single negative review will not negate a consistent history of successful complicated moves by a moving company. That’s why some moving companies who provide good service can rise and remain on top.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="about-section-3 padder">
        <img class="like-review" src="{{asset('frontend')}}/img/like-review.png" alt="">
        <div class="container">
            <div class="row">
                <div class="col-md-3 offset-md-6 col-sm-6 ">
                    <h2>9000+</h2>
                    <p>Reviews</p>
                </div>
                <div class="col-md-2 col-sm-6">
                    <h2>3000+</h2>
                    <p>Moving Businesses Reviewed</p>
                </div>
            </div>
        </div>
    </section>
</main>
@stop
@section('js')
<script type="text/javascript">

    $(document).ready(function() {
        $('select:not(.not-nice)').niceSelect();
    });

</script>
@stop
