@extends('frontend.layout')
@section('title','Mover Login')
@section('css')
@stop
@section('content')
<main role="main" class="contents">
    <section class="login-wrapper padder">
        <div class="container">
            <div class="section-title text-center">
                <h2>Log-in To View and Create Mover Reviews</h2>
            </div>
            <div class="form-wrapper">
                <a class="login-with facebook" href="{{ url('/auth/redirect/facebook') }}" title=""><i class="fa fa-facebook-square"></i>Continue with Facebook</a>
                <a class="login-with gmail" href="#" title=""><i class="fa fa-google-plus"></i>Log in using Gmail</a>
                <div class="site-log">
                    <h2>or log in using e-mail</h2>
                   <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                   {{ csrf_field() }}  
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control" placeholder="Enter Email" name="email" value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="form-text text-danger">
                                <small>{{ $errors->first('email') }}</small>
                            </span>
                        @endif
                    </div>
                     <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control" Placeholder="Enter Password" name="password" required>

                        @if ($errors->has('password'))
                            <span class="form-text text-danger">
                                <small>{{ $errors->first('password') }}</small>
                            </span>
                        @endif
                    </div>
                    <div class="form-group text-right">
                        <a href="{{URL('/forgot-password')}}" title="">Forgot Password</a>
                    </div>
                    <div class="form-group text-center">
                        <input type="submit" name="" class="btnStyle-1">
                    </div>
                    <div class="form-group text-center">
                        Don’t Have An Account Yet? <a href="{{URL('mover-registration')}}" title="">Sign Up</a>
                    </div>

                   </form> 
                </div>
            </div>
        </div>
    </section>
</main>
@stop
@section('js')
<script type="text/javascript">

    $(document).ready(function() {
        $('select:not(.not-nice)').niceSelect();
    });

</script>
@stop
