@extends('frontend.layout')
@section('title','Mover Login')
@section('css')
@stop
@section('content')
<main role="main" class="contents">
    <section class="login-wrapper padder">
        <div class="container">
            <div class="form-wrapper ">
                 <h2  class="text-center">Reset Password</h2>
				 <hr>
              <form action="{{url('reset_password/'.$user->email.'/'.$code)}}" method="post" accept-charset="utf-8">
			  @csrf
					
                            @include('frontend.includes.message')
							<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<label>Password</label>
								  <input id="password" type="password" class="form-control" name="password" required>

								@if ($errors->has('password'))
									<span class="form-text text-danger">
										<small>{{ $errors->first('password') }}</small>
									</span>
								@endif
							</div>
							<div class="form-group">
								<label>Confirm Password</label>
							  <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required>
							</div>
							<div class="form-group text-center">
								<button type="submit" name=""  class="btnStyle-1">Update password</button>
							</div>
						</form>
            </div>
        </div>
    </section>
</main>
@stop
@section('js')
<script type="text/javascript">

    $(document).ready(function() {
        $('select:not(.not-nice)').niceSelect();
    });

</script>
@stop
