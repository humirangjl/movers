@extends('frontend.layout')
@section('title','Find Mover')
@section('css')
@stop
@section('content')
<main role="main" class="contents">
    <section class="inner-banner banner">
        <img class="banner-img" src="{{asset('frontend')}}/img/find-banner.png" alt="">
        <div class="text-wrapper middle-div">
            <h2>FIND THE RIGHT MOVER FOR YOU.</h2>
            <a href="{{url('write-a-review')}}" class="btnStyle-1" title="">Write A Review</a>
        </div>
    </section>
    <section class="direct-concise padder">
        <div class="container">
            <div class="content-holder">
                <p>Welcome to Mover Reviews Direct. Our company and thousands of other people are your researchers and guides through this complicated process. With over 3000+ companies in our directory, we certainly have one that’s perfect for you. We want to give you the key to check them all out yourself, so please feel free to find the moving company that’s right for you. The written reviews will give you an insight of what to expect from a moving company so you can form your own opinion. It is our hope that your time here will assist you in finding a service that can truly move you.</p>
                <div class="icon-holder">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="image-holder">
                                <img src="{{asset('frontend')}}/img/icons/findcompany.png" alt="">
                            </div>
                            <h4>find mover</h4>
                        </div>
                        <div class="col-md-6">
                            <div class="image-holder">
                                <img src="{{asset('frontend')}}/img/icons/getreviews.png" alt="">
                            </div>
                            <h4>get reviews</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="search-wrapper">
        <div class="container">
            <form class="qoute-holder">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            <select name="year" class="form-control">
                                <option selected>Past 2 Years</option>
                                <option >Past 1 Year</option>
                                <option >Past 6 Months</option>
                                <option >Past 3 Months</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <select name="rating" class="form-control">
                                <option selected>5 Star Rating</option>
                                <option >4 Star Rating</option>
                                <option >3 Star Rating</option>
                                <option >2 Star Rating</option>
                                <option >1 Star Rating</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <input type="text" class="form-control" name="" placeholder="Search Mover Reviews">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <input type="text" class="form-control" name="" placeholder="Search a moving question">
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <input type="submit" class="btnStyle-1" name="" value="Search">
                        </div>
                    </div>
                </div>
            </form>
            <div class="show-result">Search Results - Showing 3 of 3</div>
            <div class="review-area">
                <div class="row">
                    <div class="col-xl-8 col-lg-9">
                        <div class="reviews-wrapper find-company-reviews">
                            <div class="review-item">
                                <a href="{{url('movers/mover-name')}}" class="company-details">
                                    <div class="logo-holder">
                                        <img src="{{asset('frontend')}}/img/find-logo-1.jpg" alt="">
                                    </div>
                                    <div class="company-info">
                                        <p>Brooklyn, NY based moving company:</p>
                                        <h3>Haul-All</h3>
                                        <ul class="star-rate list-inline">
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                            <li class="list-inline-item">Reviewed 67 times</li>
                                        </ul>
                                    </div>
                                </a>
                            </div>
                            <div class="review-item">
                                <a href="{{url('movers/mover-name')}}" class="company-details">
                                    <div class="logo-holder">
                                        <img src="{{asset('frontend')}}/img/clutter.png" alt="">
                                    </div>
                                    <div class="company-info">
                                        <p>Chicago, IL based moving company:</p>
                                        <h3>Clutter</h3>
                                        <ul class="star-rate list-inline">
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                            <li class="list-inline-item">Reviewed 29 times</li>
                                        </ul>
                                    </div>
                                </a>
                            </div>
                            <div class="review-item">
                                <a href="{{url('movers/mover-name')}}" class="company-details">
                                    <div class="logo-holder">
                                        <img src="{{asset('frontend')}}/img/find-logo-2.jpg" alt="">
                                    </div>
                                    <div class="company-info">
                                        <p>Frankfort, KY based moving company:</p>
                                        <h3>Flavel Moving</h3>
                                        <ul class="star-rate list-inline">
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                            <li class="list-inline-item">Reviewed 13 times</li>
                                        </ul>
                                    </div>
                                </a>
                            </div>
                            <div class="text-right">
                                <a class="paging" href="#" title="">Page 1</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-3">
                        <div class="sidebar">
                            <div class="state-movers">
                                <h2 class="side-headers">Find Movers in Your State</h2>
                                <ul class="list-unstyled">
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                            <div class="top-movers">
                                <h2 class="side-headers">Most Search Movers</h2>
                                <ul class="list-unstyled">
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                    <li><a href="#" class="" title=""><strong>Lorem Ipsum</strong> Reviews <i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@stop
@section('js')
<script type="text/javascript">

    $(document).ready(function() {
        $('select:not(.not-nice)').niceSelect();
    });

</script>
@stop
