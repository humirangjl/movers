@extends('frontend.layout')
@section('title','Review')
@section('css')
@stop
@section('content')
<main role="main" class="contents">
    <section class="review-inner-wrapper padder">
        <div class="container">
            <div class="return-review">
                <a href="{{url('movers/movers-name')}}" title=""><i class="fa fa-angle-left"></i> Back to Eagle Moving</a>
            </div>
            <div class="row">
                <div class="col-lg-9">
                    <div class="main-wrapper">
                        <div class="title-reviewer">
                            <img class="reviewer" src="{{asset('frontend')}}/img/reviewer-image.png" alt="">
                            <div class="upper">
                                <h2>“Not Sure What to Say”</h2>
                                <p>Reviewed on June 9, 2019 at 9:18PM by <strong>Mark J.</strong>, moved within Texas</p>
                            </div>
                            <ul class="star-rate list-inline">
                                <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                            </ul>
                        </div>
                        <div class="content-reviewer">
                            <p>Being the first to use this page, I don’t really have an example to follow. I guess I can just share what I thought.
                            <br><br>
                            Eagle seemed friendly enough on the phone. As the date got closer to the move. I felt they wanted my wallet more than my boxes. That is just a feeling, and I can’t say this is true or not, but let me give you an example of what I mean.
                            <br><br>
                            I get paid monthly, so my salary comes on 25th of the month. I called on the 4th of May to arrange service to move on the 30th. By the 10th, they were calling, asking me if I was sure I could pay and did I want to move to a full service. I simply couldn’t afford it and told them that. But every few days they would call. It was annoying. I almost gave up, but I figured I could handle it for a few weeks. I mean it wasn’t worse than a telemarketer. We all survive those, right?
                            <br><br>
                            Packing wasn’t that bad and I won’t bore you with the details. Finally, moving day came. 3 really big guys came to the door. I bet the neighbors thought I was going to get beaten up by a loan shark.
                            <br><br>
                            They immediately got to work and within a couple hours everything was packed. I don’t have a big place. The two men drove my car and I rode with the driver to show him my new place.
                            <br><br>
                            Not sure why I agreed to that. I should have been worried about getting my car stolen and being kidnapped. But we don’t live in that kind of an area.  Really. People even stop their cars on the road for turtles. Not to move them. To watch them. Nothing happens in our town, I guess that is partially why I am moving. I am getting married, and I can’t imagine my kids growing up a town of turtle stoppers.
                            <br><br>
                            Turns out the driver Tom, was a pastor in the town I was moving too. So much for fantasies of riding with the mob. We talked over this and that. And we arrived. Might look him up later.
                            <br><br>
                            They unpacked the truck and three road off together. It really wasn’t all that exciting. The movers were friendly and fast. I just hated being bugged for money.</p>
                            <ul class="list-inline recommended">
                                <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Verified User</label></li>
                                <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Recommended</label></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 review-area">
                    <div class="sidebar">
                        <div class="customer-service">
                            <h2 class="side-headers">Customer Service</h2>
                            <ul class="list-unstyled sb-rates">
                                <li>
                                    <span>Honesty</span>
                                    <ul class="star-rate list-inline">
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                    </ul>
                                </li>
                                <li>
                                    <span>Knowledgeable</span>
                                    <ul class="star-rate list-inline">
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                    </ul>
                                </li>
                                <li>
                                    <span>Preparedness</span>
                                    <ul class="star-rate list-inline">
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                    </ul>
                                </li>
                                <li>
                                    <span>Overall Service</span>
                                    <ul class="star-rate list-inline">
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                                        <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="write-revs text-center">
                            <h2 class="side-headers">Write your own review</h2>
                            <a href="{{url('write-a-review?mover=mover-name')}}" class="btnStyle-1" title="">Write a Review</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@stop
@section('js')
<script type="text/javascript">

    $(document).ready(function() {
        $('select:not(.not-nice)').niceSelect();
    });

</script>
@stop
