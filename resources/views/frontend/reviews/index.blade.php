@extends('frontend.layout')
@section('title','Reviews')
@section('css')
@stop
@section('content')
<main role="main" class="contents">
    <section class="inner-banner banner">
        <img class="banner-img" src="{{asset('frontend')}}/img/review-banner.png" alt="">
        <div class="text-wrapper middle-div">
            <h2>MOVING MOMENTS WITH MOVERS.</h2>
            <a href="{{url('write-a-review')}}" class="btnStyle-1" title="">Write A Review</a>
        </div>
    </section>
    <section class="direct-concise padder">
        <div class="container">
            <div class="content-holder text-center">
                <h2>Noticeboard (Reviews)</h2>
                <p>Mover Reviews Direct is open to everyone. We provide and showcase reviews with real customers who wish to voice their opinions about their personal experiences. Customers are free to say whatever they want, whenever they want but we still encourage everyone to be genuine, honest, and reasonable. These pages are updated continually, so we encourage you to visit often to find the moving company that meets your specific needs.</p>
            </div>
        </div>
    </section>
    <section class="search-wrapper">
        <div class="container">
            <form class="qoute-holder">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                            <select name="year" class="form-control">
                                <option selected>Sort By: Date</option>
                                <option >Sort By: Rating</option>
                                <option >Sort By: Reviews</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <select name="rating" class="form-control">
                                <option selected>5 Star Rating</option>
                                <option >4 Star Rating</option>
                                <option >3 Star Rating</option>
                                <option >2 Star Rating</option>
                                <option >1 Star Rating</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <input type="text" class="form-control" name="" placeholder="Search Reviews">
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <input type="submit" class="btnStyle-1" name="" value="Search">
                        </div>
                    </div>
                </div>
            </form>
            <div class="show-result">Ratings & Reports - Showing 4 of 25</div>
            <div class="champ">
                <div class="champion-item">
                    <div class="the-review">
                        <div class="title-reviewer">
                            <img src="{{asset('frontend')}}/img/reviewer-image.png" alt="">
                            <div class="upper">
                                <h2>“Not Sure What to Say”</h2>
                                <p>Reviewed on June 9, 2019 at 9:18PM by <strong>Mark J.</strong>, moved within Texas</p>
                            </div>
                        </div>
                        <div class="lower">
                            <p>Being the first to use this page, I don’t really have an example to follow. I guess I can just share what I thought. <br> Eagle seemed friendly enough on the phone. As the date got closer to the move. I felt they wanted my wallet more than my boxes. That is just a feeling, and I can’t say this is true or not, but let me give you an example of what I mean..</p>
                            <ul class="list-inline recommended">
                                <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Verified User</label></li>
                                <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Recommended</label></li>
                            </ul>
                            <a href="{{url('reviews/movers-name/review-title')}}" class="more-review btnStyle-1" title="">View Full Review</a>
                        </div>
                    </div>
                    <div class="the-info">
                        <h2>Eagle Moving</h2>
                        <ul class="star-rate list-inline">
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                            <li class="list-inline-item"><span>3.5 out of 5</span></li>
                        </ul>
                        <label>Reviewed 45 times</label>
                    </div>
                </div>
                <div class="champion-item">
                    <div class="the-review">
                        <div class="title-reviewer">
                            <img src="{{asset('frontend')}}/img/reviewer-image.png" alt="">
                            <div class="upper">
                                <h2>“Well, That Was a Mistake”</h2>
                                <p>Reviewed on June 11, 2019 at 6:57pm by <strong>Jennifer F.</strong>, moved within Texas</p>
                            </div>
                        </div>
                        <div class="lower">
                            <p>It was my mistake really. I thought the name was funny and I figured why not, the prices seems about the same as everyone else. I will tell you why not. Have you ever watched those guys at the airport handle luggage like drunken monkeys?</p>
                            <ul class="list-inline recommended">
                                <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Verified User</label></li>
                                <li class="list-inline-item"><label><i class="fa fa-times-circle"></i>Recommended</label></li>
                            </ul>
                            <a href="{{url('reviews/movers-name/review-title')}}" class="more-review btnStyle-1" title="">View Full Review</a>
                        </div>
                    </div>
                    <div class="the-info">
                        <h2>Move It!</h2>
                        <ul class="star-rate list-inline">
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                            <li class="list-inline-item"><span>2.2 out of 5</span></li>
                        </ul>
                        <label>Reviewed 105 times</label>
                    </div>
                </div>
                <div class="champion-item">
                    <div class="the-review">
                        <div class="title-reviewer">
                            <img src="{{asset('frontend')}}/img/reviewer-image.png" alt="">
                            <div class="upper">
                                <h2>“Nothing But Net”</h2>
                                <p>Reviewed on June 18, 2019 at 12:36am by <strong>Jeff W.</strong>, moved within Texas</p>
                            </div>
                        </div>
                        <div class="lower">
                            <p>I tried to do my homework, searched the net, made phone calls, everything I guess everyone does. Maybe I just got really really lucky. When I talked up a consult with my personal moving assistant (Who even talks like that?)…</p>
                            <ul class="list-inline recommended">
                                <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Verified User</label></li>
                                <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Recommended</label></li>
                            </ul>
                            <a href="{{url('reviews/movers-name/review-title')}}" class="more-review btnStyle-1" title="">View Full Review</a>
                        </div>
                    </div>
                    <div class="the-info">
                        <h2>Northern Moving Services</h2>
                        <ul class="star-rate list-inline">
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                            <li class="list-inline-item"><span>4.7 out of 5</span></li>
                        </ul>
                        <label>Reviewed 209 times</label>
                    </div>
                </div>
                <div class="champion-item">
                    <div class="the-review">
                        <div class="title-reviewer">
                            <img src="{{asset('frontend')}}/img/reviewer-image.png" alt="">
                            <div class="upper">
                                <h2>“It Was Pretty Good Year For Wine Moving”</h2>
                                <p>Reviewed on June 19, 2019 at 10:41am by <strong>Sarah J.</strong>, moved within Texas</p>
                            </div>
                        </div>
                        <div class="lower">
                            <p> I am perhaps the odd woman out. I didn’t move a home or office. I moved a wine cellar. It is a tricky thing to move wine. There are few places that move over a thousand bottles safely. Then there is the stemware, casks, etc.. It is a nightmare to even think about it…</p>
                            <ul class="list-inline recommended">
                                <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Verified User</label></li>
                                <li class="list-inline-item"><label><i class="fa fa-check-circle"></i>Recommended</label></li>
                            </ul>
                            <a href="{{url('reviews/movers-name/review-title')}}" class="more-review btnStyle-1" title="">View Full Review</a>
                        </div>
                    </div>
                    <div class="the-info">
                        <h2>Napa Specialty Moving Services</h2>
                        <ul class="star-rate list-inline">
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-active.png" alt=""></i></li>
                            <li class="list-inline-item"><img src="{{asset('frontend')}}/img/star-inactive.png" alt=""></i></li>
                            <li class="list-inline-item"><span>4.2 out of 5</span></li>
                        </ul>
                        <label>Reviewed 81 times</label>
                    </div>
                </div>
                <div class="text-right">
                    <ul class="list-inline-item">
                        <li class="list-inline-item paging">Page</li>
                        <li class="list-inline-item"><a class="paging active" href="#" title="">1</a></li>
                        <li class="list-inline-item"><a class="paging" href="#" title="">2</a></li>
                        <li class="list-inline-item"><a class="paging" href="#" title="">3</a></li>
                        <li class="list-inline-item"><a class="paging" href="#" title="">Next</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</main>
@stop
@section('js')
<script type="text/javascript">

    $(document).ready(function() {
        $('select:not(.not-nice)').niceSelect();
    });

</script>
@stop
