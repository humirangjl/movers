<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
	<!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>@yield('title') | Movers Review Direct</title>
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="152x152" href="{{asset('backend/images/icons')}}/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="{{asset('backend/images/icons')}}/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="{{asset('backend/images/icons')}}/favicon-16x16.png">
        <link rel="manifest" href="{{asset('backend/images/icons')}}/site.webmanifest">
        <link rel="mask-icon" href="{{asset('backend/images/icons')}}/safari-pinned-tab.svg" color="#249abc">
        <meta name="msapplication-TileColor" content="#4fc4f1">
        <meta name="msapplication-TileImage" content="{{asset('backend/images/icons')}}/mstile-144x144.png">
        <meta name="theme-color" content="#50c1ef">
        @include('frontend.includes.css')
	</head>
	<body>
        @include('frontend.includes.header')
        @yield('content')
        @include('frontend.includes.footer')
        @include('frontend.includes.js')
	</body>
</html>
