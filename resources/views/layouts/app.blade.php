<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('backend/images/icons')}}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('backend/images/icons')}}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('backend/images/icons')}}/favicon-16x16.png">
    <link rel="manifest" href="{{asset('backend/images/icons')}}/site.webmanifest">
    <link rel="mask-icon" href="{{asset('backend/images/icons')}}/safari-pinned-tab.svg" color="#249abc">
    <meta name="msapplication-TileColor" content="#4fc4f1">
    <meta name="msapplication-TileImage" content="{{asset('backend/images/icons')}}/mstile-144x144.png">
    <meta name="theme-color" content="#50c1ef">
    <title>@yield('title')</title>
    @php
        $configuration = \App\Models\Configuration::find(1);
        $default_settings = \App\Models\DefaultSetting::find(1);
    @endphp
    <!-- Styles -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <style>
        .bgOpacity {
            background-color: #000;
            background-image: url("{{asset('uploads/cover/1/'.$default_settings->cover)}}");
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center;
        }
        .bgOpacity:after {
            content: "";
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0;
            top: 0;
            background-color: #000;
            opacity: 0.8;
        }
    </style>
</head>

<body class="app">
    @include('admin.partials.spinner')
    <div class="peers ai-s fxw-nw h-100vh">
        <div class="bgOpacity d-n@sm- peer peer-greed h-100 pos-r">
            <div class="pos-a centerXY" style="z-index: 1000">
                <div class="pos-r" style='width: 50vw;height: 50vh;'>
                    <img class="pos-a centerXY" src="{{asset('uploads/logo/'.$configuration->logo)}}" alt="">
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4 peer pX-40 pY-80 h-100 bgc-white scrollable pos-r" style='min-width: 320px;'>
            @yield('content')
        </div>
    </div>
    @if(Request::segment(2) == 'content-management')
    <script src="{{ mix('/backend/js/app.js') }}"></script>
    @yield('js')
    @endif
</body>

</html>
