import * as $ from 'jquery';
import 'bootstrap-select';
import 'ajax-bootstrap-select';

export default (function () {
    var options = {
        ajax: {
            url: '/admin/review-management/create',
            type: "get",
            dataType: "json",
            // Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
            // automatically replace it with the value of the search query.
            data: {
                q: "{{{q}}}"
            }
        },
        locale: {
            emptyTitle: "Select and Begin Typing"
        },
        log: 3,
        preprocessData: function (data) {
            var l = data.length,
                array = [];
            if (l) {
                $.each(data, function (indexInArray, valueOfElement) {
                    array.push(
                        $.extend(true, data[indexInArray], {
                            text: valueOfElement.name,
                            value: valueOfElement.id,
                            data: {
                                subtext: valueOfElement.state
                            }
                        })
                    );
                });
            }
            // You must always return a valid array when processing data. The
            // data argument passed is a clone and cannot be modified directly.
            return array;
        }
    };

    $(".selectpicker")
        .selectpicker()
        .filter(".with-ajax")
        .ajaxSelectPicker(options);
    $("select").trigger("change");
}());
